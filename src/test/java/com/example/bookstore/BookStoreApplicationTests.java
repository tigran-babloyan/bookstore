package com.example.bookstore;

import com.example.bookstore.web.controller.AuthController;
import com.example.bookstore.web.request.CreateAuthorRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(AuthController.class)
class BookStoreApplicationTests {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void test_get_unknown_author() throws Exception {
        mockMvc.perform(get("/api/v1/authors/{id}", "sds")).andExpect(status().isNotFound());
    }

    @Test
    void test_create_author() throws Exception {
        var request = new CreateAuthorRequest("Tigran", "Babloyan");
        mockMvc.perform(post("/api/v1/authors").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated()).andExpect(jsonPath("$.author.firstName", is("Tigran")))
                .andExpect(jsonPath("$.author.lastName", is("Babloyan")))
                .andExpect(jsonPath("$.author.id", notNullValue()));
    }

}
