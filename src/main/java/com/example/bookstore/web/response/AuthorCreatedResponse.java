package com.example.bookstore.web.response;

import com.example.bookstore.model.Author;

public record AuthorCreatedResponse(Author author) {
}
