package com.example.bookstore.web.response;

import com.example.bookstore.model.Book;

public record BookCreatedResponse(Book book) {
}
