package com.example.bookstore.web.response;

import com.example.bookstore.model.Book;

public record RankedResponse(Book book) {
}
