package com.example.bookstore.web.request;

public record RankRequest(Integer ranking) {
}
