package com.example.bookstore.web.request;

public record UpdateBookRequest(String title, String authorId) {
}
