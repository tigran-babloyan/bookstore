package com.example.bookstore.web.request;

public record UpdateAuthorRequest(String firstName, String last) {
}
