package com.example.bookstore.web.request;

public record CreateAuthorRequest(String firstName, String last) {
}
