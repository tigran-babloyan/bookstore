package com.example.bookstore.web.request;

public record CreateBookRequest(String title, String authorId) {
}
