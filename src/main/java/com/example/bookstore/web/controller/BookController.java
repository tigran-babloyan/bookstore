package com.example.bookstore.web.controller;

import com.example.bookstore.model.Book;
import com.example.bookstore.web.request.*;
import com.example.bookstore.web.response.BookCreatedResponse;
import com.example.bookstore.web.response.RankedResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {

    private static final Map<String, Book> CACHE = new ConcurrentHashMap<>();


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<BookCreatedResponse> createAuthor(@RequestBody CreateBookRequest request){
        var author = AuthController.CACHE.get(request.authorId());
        var book = new Book(UUID.randomUUID().toString(), request.title(), author);
        CACHE.putIfAbsent(book.id(), book);
        return ResponseEntity.created(URI.create("/api/v1/books/" + book.id())).body(new BookCreatedResponse(book));
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Book> updateBook(@PathVariable("id")String id, @RequestBody UpdateBookRequest request){
        var author = AuthController.CACHE.get(request.authorId());
        var book = new Book(UUID.randomUUID().toString(), request.title(), author);
        CACHE.put(book.id(), book);
        return ResponseEntity.ok(book);
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Book> getBook(@PathVariable("id") String id){
        return ResponseEntity.of(Optional.ofNullable(CACHE.get(id)));
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Book> deleteBook(@PathVariable("id") String id){
        var book = CACHE.remove(id);
        return ResponseEntity.of(Optional.of(book));
    }


    @PostMapping(value = "{id}/rank", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<RankedResponse> createAuthor(@PathVariable("id")String id, @RequestBody RankRequest rankRequest){
        return ResponseEntity.ok(null);
    }

}
