package com.example.bookstore.web.controller;

import com.example.bookstore.model.Author;
import com.example.bookstore.web.request.CreateAuthorRequest;
import com.example.bookstore.web.request.UpdateAuthorRequest;
import com.example.bookstore.web.response.AuthorCreatedResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthController {

    static final Map<String, Author> CACHE = new ConcurrentHashMap<>();

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AuthorCreatedResponse> createAuthor(@RequestBody CreateAuthorRequest request){
        var author = new Author(UUID.randomUUID().toString(), request.firstName(), request.last());
        CACHE.putIfAbsent(author.id(), author);
        return ResponseEntity.created(URI.create("/api/v1/authors/" + author.id())).body(new AuthorCreatedResponse(author));
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Author> updateAuthor(@PathVariable("id")String id, @RequestBody UpdateAuthorRequest request){
        var author = new Author(id, request.firstName(), request.last());
        CACHE.put(author.id(), author);
        return ResponseEntity.ok(author);
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Author> getAuthor(@PathVariable("id") String id){
        return ResponseEntity.of(Optional.ofNullable(CACHE.get(id)));
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Author> deleteAuthor(@PathVariable("id") String id){
        var author = CACHE.remove(id);
        return ResponseEntity.of(Optional.of(author));
    }

}
