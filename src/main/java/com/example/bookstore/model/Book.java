package com.example.bookstore.model;

public record Book(String id, String title, Author author) {
}
