package com.example.bookstore.model;

public record Author(String id, String firstName, String lastName) {
}
